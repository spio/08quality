# Temat
Analiza pokrycia kodu testami.

# Cel
Celem zajęć jest analiza jakości przypadków testowych, w szczególności z wykorzystaniem techniki mierzenia pokrycia kodu testami oraz przedstawienie testowania mutacyjnego. W ramach zajęć zostaną wykonane ćwiczenia polegające na pomiarze i analizie pokrycia kodu testami, mutowaniu testów oraz ocenie korelacji mutacji z pokryciem kodu testami. W ramach zajęć zostaną wykorzystane narzędzia: JUnit oraz Pitest (http://pitest.org/).

# Technologie
* Java 8,
* JUnit 4.x, 5.x - https://junit.org/junit5/docs/current/user-guide/
* AssertJ - http://joel-costigliola.github.io/assertj/
* Pitest - http://pitest.org/

## Zadania

### Wprowadzenie
Testowanie mutacyjne służy przede wszystkim ocenie jakości przypadków testowych. Do kodu produkcyjnego wprowadzane są modyfikacje (mutacje), po czym ponownie wykonuje się przypadki testowe. Test dobrej jakości powinien wykryć zmutowany kod.
Jeśli testowanie mutacyjne wykaże, że dany przypadek testowy jest mało skuteczny to należy przeprojektować test lub dodać kolejny wariant, pokrywający ten przypadek.

### Treść
W ramach zadania należy przeprowadzić analizę wyników wygenerowanych narzędziem PIT dla dwóch projektów napisanych w języku Java. Projekty zawierają ten sam kod bazowy, ale różnią się kodem testów jednostkowych. Wynikiem analizy powinno być podsumowanie z wnioskami, do przedyskutowania na zajęciach.
W ramach analizy należy przejrzeć wyniki pokrycia kodu testami (code coverage).

Projekt: https://gitlab.com/spio-sources/stack/tree/master/jstack

Testy do analizy: https://gitlab.com/spio-sources/stack/tree/master/jstack/stack-quality

